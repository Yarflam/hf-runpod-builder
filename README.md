# HuggingFace Runpod Builder

## Build the image

```bash
sudo docker build --build-arg POD_PROD_MODEL --build-arg POD_PROD_SUPPORT=basic --build-arg POD_DOWNLOAD=yes --tag myimage:latest .
```

## Author

- *Yarflam*