from pytorch/pytorch:2.1.2-cuda12.1-cudnn8-devel
LABEL vendor="Yarflam"
ARG DEBIAN_FRONTEND=noninteractive
ARG POD_CACHE=weights
ARG POD_TARGET=prod
ARG POD_PROD_MODEL
ARG POD_PROD_SUPPORT
ARG POD_DOWNLOAD

# Install
WORKDIR /var/usr/app
COPY ./requirements.txt ./requirements.txt
RUN pip install -U pip
RUN pip install -r requirements.txt
RUN pip install -U bitsandbytes

# Copy and generate the caches
COPY ./pod.py ./pod.py
RUN python pod.py

CMD python pod.py