from transformers import AutoTokenizer, AutoModelForCausalLM, BitsAndBytesConfig
from peft import PeftConfig, PeftModel
from huggingface_hub import snapshot_download
from dotenv import load_dotenv
import runpod
import torch
import json
import os
load_dotenv()

class PodModel:
    OPTIONS = {
        'max_new_tokens': {
            'type': [ int ],
            'default': 20,
            'min': 15,
            'max': 1024
        },
        'n': {
            'type': [ int ],
            'default': 1,
            'min': 1,
            'max': 3
        },
        'do_sample': {
            'type': [ bool ],
            'default': True
        },
        'temperature': {
            'type': [ int, float ],
            'default': 0.7,
            'min': 0,
            'max': 1
        },
        'top_p': {
            'type': [ int, float ],
            'default': 0.9,
            'min': 0,
            'max': 1
        },
        'stop_text': {
            'type': [ list ],
            'default': [ '(Note', '[Note', '[[' ]
        }
    }

    def __init__(self, model_name, cache_path) -> None:
        self.device = 'cuda' if torch.cuda.is_available() else 'cpu'
        self.model_name = model_name
        self.cache_path = cache_path
        self.model = None
        self.tokenizer = None
    def setup(self, dependency=None) -> None:
        snapshot_download(repo_id=self.model_name, cache_dir=self.cache_path)
        if '/' in dependency: snapshot_download(repo_id=dependency, cache_dir=self.cache_path)
    def to(self, device=None) -> None:
        if self.model == None: return
        self.model.to(self.device if device == None else device)
    def secureOptions(self, options={}) -> dict:
        base = PodModel.OPTIONS
        if not type(options) == dict: options = {}
        for key in base.keys():
            if not key in options.keys():
                options[key] = None
            # TYPE
            if not type(options[key]) in base[key]['type']:
                options[key] = base[key]['default']
            # Number
            if type(options[key]) in [ int, float ]:
                # MIN
                if options[key] < base[key]['min']:
                    options[key] = base[key]['min']
                # MAX
                if options[key] > base[key]['max']:
                    options[key] = base[key]['max']
        return options
    def generate(self, prompt, options={}) -> list:
        try:
            chat = json.loads(prompt)
        except:
            chat = [{ 'role': 'user', 'content': prompt }]
        options = self.secureOptions(options)
        encodeds = self.tokenizer.apply_chat_template(chat, return_tensors="pt")
        size = len(encodeds[0])
        # Generate
        outputs = self.model.generate(
            encodeds.to(self.device),
            num_return_sequences=options['n'],
            max_new_tokens=options['max_new_tokens'],
            do_sample=options['do_sample'],
            temperature=options['temperature'],
            top_p=options['top_p'],
            pad_token_id=self.tokenizer.eos_token_id
        )
        return self.tokenizer.batch_decode([output[size:] for output in outputs], skip_special_tokens=True)

class PodBasicModel(PodModel):
    def __init__(self, model_name, cache_path=None, setup=None) -> None:
        PodModel.__init__(self, model_name, cache_path)
        if not setup == None: return self.setup(setup)
        self.load()
    def load(self) -> None:
        # Model
        self.model = AutoModelForCausalLM.from_pretrained(
            self.model_name,
            return_dict=True,
            device_map='auto',
            trust_remote_code=True,
            cache_dir=self.cache_path
        )
        # Tokenizer
        self.tokenizer = AutoTokenizer.from_pretrained(
            self.model_name,
            cache_dir=self.cache_path
        )
        self.tokenizer.pad_token = self.tokenizer.eos_token

class PodPeftModel(PodModel):
    def __init__(self, model_name, cache_path=None, setup=None) -> None:
        PodModel.__init__(self, model_name, cache_path)
        if not setup == None: return self.setup(setup)
        self.load()
    def load(self) -> None:
        # PEFT support
        bnb_config = BitsAndBytesConfig(
            load_in_4bit=True,
            bnb_4bit_use_double_quant=True,
            bnb_4bit_quant_type="nf4",
            bnb_4bit_compute_dtype=torch.bfloat16
        )
        peft_config = PeftConfig.from_pretrained(self.model_name, cache_dir=self.cache_path)
        # Model
        self.model = AutoModelForCausalLM.from_pretrained(
            peft_config.base_model_name_or_path,
            quantization_config=bnb_config,
            cache_dir=self.cache_path
        )
        self.model = PeftModel.from_pretrained(self.model, self.model_name, cache_dir=self.cache_path)
        # Tokenizer
        self.tokenizer = AutoTokenizer.from_pretrained(
            peft_config.base_model_name_or_path,
            cache_dir=self.cache_path
        )
        self.tokenizer.pad_token = self.tokenizer.eos_token

def myPod(llm) -> None:
    def predict(job):
        job_input = job['input']
        prompt = job_input['prompt']
        if not isinstance(prompt, str):
            return { "error": "Give a prompt parameter (string)." }
        return llm.generate(prompt, job_input)
    runpod.serverless.start({ 'handler': predict })

def main() -> None:
    setup = os.environ['POD_DOWNLOAD'] if 'POD_DOWNLOAD' in os.environ and len(os.environ['POD_DOWNLOAD']) else None
    cache_path = os.environ['POD_CACHE'] if 'POD_CACHE' in os.environ and len(os.environ['POD_CACHE']) else None
    prod = True if 'POD_TARGET' in os.environ and os.environ['POD_TARGET'] == 'prod' else False
    model_name = os.environ['POD_PROD_MODEL'] if prod else os.environ['POD_DEV_MODEL']
    support = os.environ['POD_PROD_SUPPORT'] if prod else os.environ['POD_DEV_SUPPORT']
    if support == 'peft':
        llm = PodPeftModel(model_name, cache_path, setup)
    else:
        llm = PodBasicModel(model_name, cache_path, setup)
    if setup == None: myPod(llm)

main()